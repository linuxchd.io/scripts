# Git Repository

This is how to create and push git repos in gitlab

1. Creating New Repository

	In gitlab, create a new public repository with user linuxchd.io

2. Clone new repository
```sh
git clone https://gitlab.com/linuxchd.io/(newrepo).git
```
3. Create README.md

```sh
vim README.md
```
4. Create a script for add, commit and push repository

```sh
vim gitlab.sh
```
```sh
git add .
git commit -m "update"
git push gitlab main
```
```sh
chmod +x gitlab.sh
```
5. Remote existing repository

```sh
git remote add gitlab https://gitlab.com/linuxchd.io/(newrepo).git
```

6. Launch the Script

```sh
./gitlab.sh
```
